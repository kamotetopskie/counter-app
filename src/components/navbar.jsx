import React from "react";

//Stateless FUNCTIONAL Component
//Instead having a class that extends the component class w/ the render method, we simply define a function that returns a react element that what it's called that name
//There is nothing with using classes to define components, but some developers used functions when they are dealing w/ simple stateless components

const NavBar = ({ totalCounters }) => {
  //React will pass the props object as an argument to this function at runtime
  return (
    <nav className="navbar navbar-light bg-light">
      <span className="navbar-brand">
        Navbar
        <span className="badge badge-pill badge-secondary m-2">
          {totalCounters}
        </span>
      </span>
    </nav>
  );
};

export default NavBar;
