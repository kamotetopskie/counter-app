import React, { Component } from "react";
import Navbar from "./components/navbar";
import Counters from "./components/counters"; //Since Counter is the default export, there's no need for curly braces here
import "./App.css";

class App extends Component {
  state = {
    counters: [
      { id: 1, value: 4 },
      { id: 2, value: 0 },
      { id: 3, value: 0 },
      { id: 4, value: 0 }
    ]
  };

  constructor() {
    //This constructor called only once when an instance of a class is created, this is a great opportunity for initializing properties in that instance
    //When you want to set the state directly, you can DO so in CONSTRUCTOR BUT you need to pass the props as a parameter to constructor and in super Ex. this.state = this.props.something;
    super(); //Once you call a constructor, it's a MUST to call this CONSTRUCTOR OF THE PARENET ELEMENT which is the super();
    console.log("App - Constructor");
  }

  componentDidMount() {
    //This method is called after our component is rendered into the DOM and the PERFECT place to make AJAX calls to get data from the server
    console.log("App - Mouted");
  }

  handleIncrement = counter => {
    const counters = [...this.state.counters];
    const index = counters.indexOf(counter);
    counters[index] = { ...counter };
    counters[index].value++;
    this.setState({ counters });
  };

  handleDecrement = counter => {
    const counters = [...this.state.counters];
    const index = counters.indexOf(counter);
    counters[index] = { ...counter };
    counters[index].value--;
    this.setState({ counters });
  };

  handleDelete = counterId => {
    const counters = this.state.counters.filter(c => c.id !== counterId); //We created a new constant that will hold the counters array without the one with has the same id as the parameter passed to it.
    this.setState({ counters });
  };

  handleReset = () => {
    //We map each element in the counter array and set its value to 0 then reassign it to counters variable
    const counters = this.state.counters.map(c => {
      c.value = 0;
      return c;
    });
    this.setState({ counters });
  };

  render() {
    console.log("App - Rendered");

    return (
      <React.Fragment>
        <Navbar
          totalCounters={this.state.counters.filter(c => c.value > 0).length}
        />
        <main className="container">
          <Counters
            counters={this.state.counters}
            onReset={this.handleReset}
            onIncrement={this.handleIncrement}
            onDecrement={this.handleDecrement}
            onDelete={this.handleDelete}
          />
        </main>
      </React.Fragment>
    );
  }
}

export default App;
